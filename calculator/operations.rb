require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist) 
      given_grades = JSON.parse(grades)

      sum = 0
      count = 0

      given_grades.each do |nome,nota|
        if(not blacklist.include?(nome))
          sum += nota
          count += 1
        end
      end

      return (sum/count).to_f
    end
  
    def no_integers(numbers)
      numbers = numbers.split (" ")
       divisao = ["25","50","75","00"]
       result = ""
       numbers.each do |numero|
        if(divisao.include?(numero[-2..-1]))
          result+="S "
        else
          result+="N "
        end
      end
      return result
    end
  
    def filter_films(genres, year)
      films = get_films[:movies]
      genres = genres.split(" ")

      ans = []

      films.each do |movie|
        if(movie[:year].to_i >= year && (movie[:genres] & genres).length == genres.length)
          ans.push(movie[:title])
        end
      end

      return ans

    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end
