require_relative 'operations'
require_relative '../extra_operations'

module Calculator
  class Menu
    include ExtraOperations

    def initialize
      
      operations = Operations.new
      extra_operations = Extra.new
     

      puts "Bem vindo a calculadora mais show das galáxias"
      puts "Digite a operação desejada para iniciarmos"
      puts "1- Média Preconceituosa"
      puts "2- Calculadora sem números"
      puts "3- Filtrar filmes"
      puts "4- Adivinhe o número"
      puts "0- Encerrar Sessão"

      op = gets.chomp.to_i
      case op
      when 1

        puts "Digite o JSON com as notas:"
        grades = gets.chomp
        puts "Digite a lista dos usuarios a serem ignorados:"
        blacklist = gets.chomp

        avg = operations.biased_mean(grades,blacklist)
        puts "Nota Final é igual a #{avg}"

      when 2
        puts "Digite uma sequência de números e informaremos se eles são divisíveis ou não por 25"
        numbers = gets.chomp

        result = operations.no_integers(numbers)
        puts "#{result}"
      
      when 3
        puts "Vamos fazer uma pesquisa sobre filme. Digite gênero(s) em inglês para filtrar os filmes"
        genres = gets.chomp

        puts "Agora digite o ano em que ele foi lançado"
        year = gets.chomp.to_i
        
        movies = operations.filter_films(genres,year)
        puts movies
      
      when 4
        

        puts "Leia atentamente as informações sobre um número e tente descobri-lo."
        puts "1. É um número inteiro e positivo"
        puts "2. É um número par"
        puts "3. É um número menor que 45"
        puts "4. Alguns de seus divisores são 1,2,3 e 8"
        puts "5. Ele não é divisor de 60"

        number = gets.chomp.to_i

        answer = extra_operations.whats_thenumber(number)
        puts answer

      when 0
        puts "calculadora encerrada"
        exit

      else 
        puts "digite um número válido"

      end
    end
  end
end
